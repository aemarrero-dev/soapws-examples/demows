EJEMPLO DE CONSUMO DE SERVICIO SOAP.

COMO SE HIZO:

1. A PARTIR DEL WSDL SE IMPORTARON LAS CLASES CON WSIMPORT.

wsimport -p net.banking -clientjar BLZService-1.0.0.jar http://www.thomas-bayer.com/axis2/services/BLZService?wsdl -extension

2. INSTALAMOS JAR EN MAVEN MEDIANTE MVN INSTALL

mvn install:install-file -Dfile=BLZService-1.0.0.jar -DgroupId=net.banking -DartifactId=banking-data -Dversion=1.0.0 -Dpackaging=jar

3. DOCUMENTACION UTILIZADA COMO GUIA:

http://blog.ricardo.studio/2018/04/27/consumo-servicio-soap-spring-boot/#:~:text=Para%20poder%20consumir%20un%20servicio,y%20%22respuesta%22%20del%20servicio.

https://bank-code.net/blz-sort-codes   (CODIGOS PRUEBA DEL SERVICIO CONSUMIDO)

https://stackoverflow.com/questions/41633782/consume-soap-ws-in-spring-mvc

https://medium.com/@Jhonjairochac/consumir-un-servicio-soap-con-spring-boot-238a9e162674

4. SE ADJUNTA ESTRUCTURA DE CARPETAS CREADA EN MAVEN. (net.jar)

