package com.soap.demows.client;

import com.sun.xml.internal.ws.developer.WSBindingProvider;
import net.banking.*;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import com.soap.demows.config.SoapExecutionCallBack;
import org.springframework.ws.soap.SoapElement;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import java.util.List;

public class DemowsClient extends WebServiceGatewaySupport {

    public GetBankResponseType getBankDetail(String blz) {
        GetBankType request = new GetBankType();
        request.setBlz(blz);

        ObjectFactory of = new ObjectFactory();
        JAXBElement<GetBankType> reqjaxb = of.createGetBank(request);

        @SuppressWarnings("unchecked")
        JAXBElement<GetBankResponseType> response = (JAXBElement<GetBankResponseType>) getWebServiceTemplate()
                .marshalSendAndReceive(
                        "http://www.thomas-bayer.com/axis2/services/BLZService",
                        reqjaxb,
                        new SoapExecutionCallBack("http://thomas-bayer.com/blz/BLZService/getBankResponse"));

        return response.getValue();

    }

    public void consumeWS() throws SOAPException {
        BLZService service = new BLZService();
        BLZServicePortType port = service.getBLZServiceSOAP11PortHttp();
        String url = "http://www.thomas-bayer.com/axis2/services/BLZService";

        WSBindingProvider provider = (WSBindingProvider) port;
        provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);

        //SOAPMessage message = null;
        //message = MessageFactory.newInstance().createMessage();

        //GetBankType request = new GetBankType();
        //request.setBlz("12070000");
        GetBankResponseType response = new GetBankResponseType();

        //List<Handler> handlerChain = provider.getBinding().getHandlerChain();
        //handlerChain.add(new SOAPLoggingHandler)
        response.setDetails(port.getBank("12070000"));

        System.out.println("DIRECT CONSUMING: " + response.getDetails().getBezeichnung());
    }
}
