package com.soap.demows.config;

import com.soap.demows.client.DemowsClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class GetBankDetailWSConfig {

    @Bean
    public Jaxb2Marshaller getBankDetailsMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("net.banking");
        return marshaller;
    }

    @Bean
    public DemowsClient getBankDetailWSClient(Jaxb2Marshaller marshaller) {
        DemowsClient client = new DemowsClient();
        client.setDefaultUri("http://www.thomas-bayer.com/axis2/services/BLZService");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}