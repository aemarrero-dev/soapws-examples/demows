package com.soap.demows.controller;

import com.soap.demows.service.GetBanksServiceImpl;
import lombok.extern.log4j.Log4j2;
import net.banking.DetailsType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping("/ws")
public class DemowsController {

    @Autowired
    private GetBanksServiceImpl getBanksService;

    @GetMapping("/detail")
    public DetailsType index(@RequestParam String blz) {
        log.info("request to /ws ");
        DetailsType response = getBanksService.getBanksDetail(blz);
        log.info("response: " + response.getBezeichnung() + ", " + response.getBic() + ", " + response.getOrt() + ", " + response.getPlz());
        return response;
    }

    @GetMapping("/bez")
    public String getBezeichnung(@RequestParam String blz) {
        log.info("request to /ws ");
        String response = getBanksService.getBezeichnung(blz);
        log.info("response: " + response);
        return response;
    }
}
