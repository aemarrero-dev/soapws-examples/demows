package com.soap.demows.service;

import net.banking.DetailsType;

public interface GetBanksService {

    public DetailsType getBanksDetail(String blz);
    public String getBezeichnung(String blz);
}
