package com.soap.demows.service;

import com.soap.demows.client.DemowsClient;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import net.banking.DetailsType;

import javax.xml.soap.SOAPException;

@Service
@Log4j2
public class GetBanksServiceImpl implements GetBanksService{

    @Autowired
    private DemowsClient wsClient;

    @Override
    public DetailsType getBanksDetail(String blz) {
        log.info("getBankDetail");
        return wsClient.getBankDetail(blz).getDetails();
    }

    @Override
    public String getBezeichnung(String blz) {
        log.info("getBezeichnung");

        try {
            wsClient.consumeWS();
        } catch (SOAPException e) {
            e.printStackTrace();
        }
        return wsClient.getBankDetail(blz).getDetails().getBezeichnung();
    }
}
